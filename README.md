![Sample](screenshot.png)

## How to put this to your web page

Load stylesheets:

```css
@import url("https://kasmide.gitlab.io/fork_me_on_gitlab/style.min.css");

/* If you want to enable dark mode support ;
   Note: File style.dark.min.css just overwrite the bg/fg color and doesn't work alone */
@media (prefers-color-scheme: dark) {
  @import url("https://kasmide.gitlab.io/fork_me_on_gitlab/style.dark.min.css");
}
```

Put the code below to your website:

```html
<a href="https://link.to/your/repo/">
  <div id="gitlab_banner">
    <img
      src="https://about.gitlab.com/images/press/logo/svg/gitlab-icon-1-color-black-rgb.svg"
      alt="logo"
    />
    <p>Fork me on GitLab</p>
  </div>
</a>
```

## How to customize the banner

### Change logo image

```diff
 <a href="https://link.to/your/repo/">
   <div id="gitlab_banner">
-    <img src="https://about.gitlab.com/images/press/logo/svg/gitlab-icon-1-color-black-rgb.svg" />
+    <img src="https://example.org/path/to/custom/image.svg" />
     <p>Fork me on GitLab</p>
   </div>
 </a>
```

### Change background color

Load a stylesheet like the sample below:

```css
#gitlab_banner {
  background: #YourFavoriteColor;
}
```
